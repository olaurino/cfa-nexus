import * as crypto from "crypto";
import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import * as helm from "@pulumi/kubernetes/helm";
import * as fs from "fs";
import * as yaml from "yaml";
import * as _ from "lodash"
import {storage} from "@pulumi/kubernetes";

const generateToken = (length: number): string => {
    return crypto.randomBytes(length).toString("hex");
};

const config = new pulumi.Config();
const vaultChartLocation = config.get("vaultChartLocation") || "../services/vault-secrets-operator";
const vaultToken = config.requireSecret("vaultToken");
const vaultAddress = config.requireSecret("vaultAddress");

const nginxChartLocation = config.get("nginxChartLocation") || "../services/ingress-nginx";
const nginxValues = config.getObject<string[]>('nginxValues')
const argoCdValues = config.getObject<string[]>('argoCdValues')

const clusterType = config.require("clusterType")

// export let rookCephChart: helm.v3.Chart
// export let storageClass: pulumi.Output<storage.v1.VolumeAttachmentList>
//
// if (clusterType == "vagrant") {
//     const namespace = new k8s.core.v1.Namespace("rook-ceph-ns", {
//         metadata: {name: "rook-ceph"},
//     })
//
//     rookCephChart = new helm.v3.Chart("rook-ceph-block-storage", {
//         chart: "rook-ceph",
//         version: "1.11.0",
//         fetchOpts: {
//             repo: "https://charts.rook.io/release",
//         },
//         namespace: namespace.metadata.name,
//         values: {
//             cephBlockPool: {
//                 pools: [{
//                     name: "replicapool",
//                     failureDomain: "osd",
//                     replicated: {
//                         size: 1,
//                         requireSafeReplicaSize: false,
//                     },
//                 }],
//             },
//             storageClass: {
//                 csi: {
//                     rbd: {
//                         provisioner: "rook-ceph.rbd.csi.ceph.com",
//                         imageFormat: "2",
//                         imageFeatures: "layering",
//                         pool: "replicapool",
//                         allowVolumeExpansion: true,
//                         reclaimPolicy: "Delete",
//                         cephConfigSecret: "rook-ceph-config",
//                         cephSecretNamespace: namespace.metadata.name,
//                     },
//                 },
//             },
//         },
//     });
//
//     storageClass = rookCephChart.getResource("storage.k8s.io/v1/VolumeAttachmentList", "rook-ceph-block");
// }

export const vaultOperatorNs = new k8s.core.v1.Namespace("vault-secrets-operator-ns", {
    metadata: {name: "vault-secrets-operator"},
});

export const vaultSecret = new k8s.core.v1.Secret("vault-secrets-operator-secret", {
    metadata: {
        name: "vault-secrets-operator",
        namespace: vaultOperatorNs.metadata.name,
    },
    data: {
        "VAULT_TOKEN": pulumi.secret(vaultToken).apply(v => Buffer.from(v).toString("base64")),
        "VAULT_TOKEN_LEASE_DURATION": pulumi.secret("31536000").apply(s => Buffer.from(s).toString("base64")),
    },
    type: "Opaque",
});

export const vaultOperatorRelease = new helm.v3.Release("vault-secrets-operator", {
    chart: vaultChartLocation,
    namespace: vaultOperatorNs.metadata.name,
    dependencyUpdate: true,
    values: {
        "vault-secrets-operator": {
            "environmentVars": [
                {
                    "name": "VAULT_TOKEN",
                    "valueFrom": {
                        "secretKeyRef": {
                            "name": "vault-secrets-operator",
                            "key": "VAULT_TOKEN",
                        },
                    },
                },
                {
                    "name": "VAULT_TOKEN_LEASE_DURATION",
                    "valueFrom": {
                        "secretKeyRef": {
                            "name": "vault-secrets-operator",
                            "key": "VAULT_TOKEN_LEASE_DURATION",
                        },
                    },
                },
            ],
            "vault": {
                "address": vaultAddress,
                "reconciliationTime": 60,
            },
        },
    },
}, {dependsOn: [vaultOperatorNs, vaultSecret]});

const ingressNginx = "ingress-nginx"

export function mergeYamlFiles(paths: string[]): any {
    const sources = paths.map((path) => {
        const content = fs.readFileSync(path, "utf8");
        return yaml.parse(content);
    });

    return _.merge({}, ...sources);
}

export const nginxIngress = new k8s.helm.v3.Release(ingressNginx, {
    chart: nginxChartLocation,
    createNamespace: true,
    namespace: ingressNginx,
    dependencyUpdate: true,
    values: mergeYamlFiles(nginxValues || [])
});

const argoCd = "argocd"

export const argoCdNs = new k8s.core.v1.Namespace("argocd-ns", {
    metadata: {name: argoCd},
});

export const argoCdSecret = new k8s.core.v1.Secret("argocd-secret", {
    metadata: {
        name: "argocd-secret",
        namespace: argoCd,
    },
    type: "Opaque",
}, {dependsOn: argoCdNs});

export const argoCdRelease = new k8s.helm.v3.Release(argoCd, {
    chart: "../services/argocd",
    namespace: argoCd,
    values: {
        ...mergeYamlFiles(argoCdValues || []),
        global: {
            vaultSecretsPath: "secret/k8s_operator/cfa-nexus.cloud"
        }
    },
    dependencyUpdate: true,
}, {dependsOn: [argoCdSecret, nginxIngress]})
