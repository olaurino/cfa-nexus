import * as bcrypt from 'bcrypt';
import * as readline from 'readline';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Enter a string to bcrypt: ', async (stringToBcrypt: string) => {
    const saltRounds = 10;
    const hashedString = await bcrypt.hash(stringToBcrypt, saltRounds);
    console.log(`The bcrypt hash of "${stringToBcrypt}" is: ${hashedString}`);
    rl.close();
});
