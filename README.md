Development Cluster
-------------------

Set up a `kubernetes` cluster using `vagrant`, with a `controller` node and a number of `worker` nodes. The 
`Vagrantfile` should be self-explanatory, and you can configure the number of nodes to install.

To set up the cluster you'll need `vagrant` and at least one virtualization provider. `VirtualBox` is a common choice,
and at this time the only one tested. I quickly tested it with `Hyper-V` but `MetalLb` would not be able to allocate 
an IP pool. I did not explore the issue any further and switched back to `VirtualBox`.

The advantage of using `vagrant` rather than `minikube` is that `minikube` hides a significant amount of complexity, 
so its configuration cannot be applied to real deployments.

I use `microk8s`. To some extent this tool also hides the complexity of some deployment options. However, unlike 
`minikube`, it is a production-ready, enterprise grade `kubernetes` implementation, so in principle it could be used
in production.

Instructions
------------

```bash
vagrant up
```

The configuration sets up port forwarding for `8888`, which can be used
to expose services to the host OS.