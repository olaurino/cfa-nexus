set -e
#sudo setenforce 0
#sudo sed -i 's/^SELINUX=.*/SELINUX=permissive/g' /etc/selinux/config
#sudo firewall-cmd  --permanent --add-port={10255,12379,25000,16443,10250,10257,10259,32000}/tcp
#sudo firewall-cmd --reload
#yum install -y epel-release
#yum install -y snapd
#test -h /snap || ln -s /var/lib/snapd/snap /snap
#systemctl enable --now snapd.socket
#snap wait system seed.loaded
snap install microk8s --classic
snap install docker
usermod -a -G microk8s vagrant
echo "alias kubectl='microk8s.kubectl'" > /home/vagrant/.bash_aliases
chown vagrant:vagrant /home/vagrant/.bash_aliases
echo "alias kubectl='microk8s.kubectl'" > /root/.bash_aliases
chown root:root /root/.bash_aliases